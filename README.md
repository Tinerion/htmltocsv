# htmltocsv

Une application textuelle en python pour convertir des pages html en un fichier CSV

# Éxécution :

Télécharger script.py
Le placer dans un dossier contenant : 
 - Un fichier linkList.txt contenant tout les liens qui doivent être exportés, sous la forme "https://www.example.com/chemin/"
 - Un sous dossier nommé www.example.com contenant toutes vos pages avec la même arborescence que sur votre site web
        (Exemple : pour extraire la page www.example.com/page/index.html, la page doit se trouver dans le sous dossier www.example.com,
        contenant lui même le sous-dossier page, contenant le fichier index.html)

*Sur Windows :* 

Lancer le script en double cliquant simplement dessus

*Sur Linux :*

Dans un terminal, rendez vous dans le dossier contenant script.py, et rendez-le éxecutable grâce à la commande : ```chmod +x script.py```
Puis éxécutez le avec la commande : ```./script.py```
