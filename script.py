#!/usr/bin/env python3.7
# Scrit html to csv
# But : Créer un doc CSV contenant les liens, titres et contenu de pages web
# Auteur : Jérémy [Tinerion] Duhamel
# Licence : GPL

# Imported Library
import csv  # To create or manipulate csv file
from urllib.parse import urlparse # To manipulate and parse url
from bs4 import BeautifulSoup

# Variables
sitemapLineScrolled = 0
nbLinks = 0
pageExtracted = ""
post_title = ""
post_date = ""
post_name = ""
post_content = ""
post_status = "draft"
post_type = "post"
post_category = "blog"
post_url = ""
post_author = ""

with open('export.csv', 'w') as csvFile:
    fieldnames = ['post_title', 'post_date', 'post_name', 'post_content', 'post_status', 'post_type', 'post_category', 'post_url', 'post_author']
    writer = csv.DictWriter(csvFile, fieldnames=fieldnames)
    writer.writeheader()

    # Open the link list
    with open('linkList.txt', 'r') as file:

        # Scroll the page to import
        for link in file:
            urlParsed = urlparse(link)
            post_url = link
            pageExtracted = urlParsed.netloc + urlParsed.path + 'index.html'
            pageExtracted = pageExtracted.replace("\n", "")
            with open(pageExtracted) as page:
                soup = BeautifulSoup(page, "lxml")
                post_title = soup.title.string
                post_date = soup.find("span", class_="j-text j-blog-post--date").string
                post_name = soup.h1.string
                pageContent = soup.find_all("p")
                post_content = soup.p(pageContent.text)
                writer.writerow({'post_title': post_title, 'post_date': post_date, 'post_name': post_name, 'post_content': post_content, 'post_status': post_status, 'post_type': post_type, 'post_category': post_category, 'post_url': post_url, 'post_author': post_author})

    
